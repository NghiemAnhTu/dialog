package com.rikkei.tranning.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;

public class DialogBasic extends DialogFragment{
    public DialogBasic() {
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder( getActivity());

        builder.setTitle("Erase USB Storage");
        builder.setMessage("You'll lose all photos and media!");
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d("DialogBasic", "Cancel");
                dialog.cancel();
            }
        });
        builder.setPositiveButton("Erase", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d("DialogBasic", "Erase");
                dialog.dismiss();
            }
        });

        return builder.create();
    }

}
