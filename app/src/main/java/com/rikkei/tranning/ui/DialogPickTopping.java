package com.rikkei.tranning.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

public class DialogPickTopping extends DialogFragment {
    String[] content = {"Onion", "Letture", "Tomato" };
    boolean[] selecedIt = new boolean[content.length];

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Pick yours topping");
        builder.setMultiChoiceItems(content, selecedIt, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if ( isChecked ){
                    selecedIt[which] = true;
                }else {
                    selecedIt[which] = false;
                }
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), "Cancel Dialog Pick Your Topping", Toast.LENGTH_LONG).show();
                dialog.cancel();
            }
        });

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String chose = "";
                for (int i = 0; i < content.length; i++ ){
                    if (selecedIt[i]){
                        chose = chose + " " + content[i];
                    }
                }
                Toast.makeText(getActivity(), "Ok Dialog Pick Your Topping" + chose, Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
        return builder.create();
    }
}
