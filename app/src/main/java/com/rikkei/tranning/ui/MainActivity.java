package com.rikkei.tranning.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /** Dialog Basic */
//        DialogBasic dialogBasic = new DialogBasic();
//        dialogBasic.show( getSupportFragmentManager(), "");

        /** Dialog Pick Your Topping */
//        DialogPickTopping dialogPickTopping = new DialogPickTopping();
//        dialogPickTopping.show(getSupportFragmentManager(), "");

        /** Dialog Brightness */
//        DialogBrightness dialogBrightness = new DialogBrightness();
//        dialogBrightness.show( getSupportFragmentManager(), "");

        /** Dialog Limit */
//        DialogLimit dialogLimit = new DialogLimit();
//        dialogLimit.show(getSupportFragmentManager(), "anh tu");

        /** Dialog Brightness */
//        DialogBrightness dialogBrightness = new DialogBrightness();
//        dialogBrightness.show( getSupportFragmentManager(), "");

        /** Dialog TimePicker */
//        DialogTimePicker dialogTimePicker = new DialogTimePicker();
//        dialogTimePicker.show(getSupportFragmentManager(), "");

        /** Dialog DatePicker */
        DialogDatePicker dialogDatePicker = new DialogDatePicker();
        dialogDatePicker.show(getSupportFragmentManager(), "");

    }
}
