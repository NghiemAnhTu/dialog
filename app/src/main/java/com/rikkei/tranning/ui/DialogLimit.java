package com.rikkei.tranning.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Toast;

public class DialogLimit extends DialogFragment {
    NumberPicker numberPicker;
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // custom xml
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.limit, null);

        // gan view cho dialog
        builder.setView(view);
        builder.setTitle("Text limit message");
        // NumbrePicker
        numberPicker = view.findViewById(R.id.numberPicker);
        numberPicker.setMaxValue(500);
        numberPicker.setMinValue(0);

        // su kien cho dialog
        builder.setPositiveButton("Set", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int num = numberPicker.getValue();
                Toast.makeText(getActivity(), "NumberPicker: " + num, Toast.LENGTH_LONG).show();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), "NumberPicker is not select", Toast.LENGTH_LONG).show();
            }
        });
        return builder.create();
    }
}
